# -*- coding: utf-8 -*-
"""
@author: ATR
"""
from spyne import Application, rpc, ServiceBase, Unicode, Integer, Iterable
from spyne.model.primitive.number import Float
from spyne.protocol.soap import Soap11
from spyne.server.wsgi import WsgiApplication


import requests
import json
from logging import info
import urllib.request, json
from geopy.geocoders import Nominatim
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


from spyne.model.primitive.string import String
from spyne.server.wsgi import WsgiApplication
from spyne.protocol.soap import Soap11
from spyne.service import ServiceBase
from spyne.application import Application
from spyne.decorator import rpc
from spyne.model.primitive import Integer, Unicode
from spyne.model.complex import Iterable
from wsgiref.simple_server import make_server
from engine import *

class service(ServiceBase):
    @rpc(list, list, _returns=Float)
    def infos(ctx, coordDest, coordSrc,):
        return duration(coordDest, coordSrc)

application = Application(
    [service],
    'spyne.examples.hello.soap',
    in_protocol=Soap11(validator='lxml'),
    out_protocol=Soap11()
)
wsgi_application = WsgiApplication(application)

server = make_server('127.0.0.1', 8080, wsgi_application)
server.serve_forever()