 # -*- coding: utf-8 -*-
"""
@author: ATR
"""
from flask import Flask
from flask_restful import Resource, Api
from flask import Flask, redirect, url_for, render_template, request, flash
from engine import *
from zeep import Client

app = Flask(__name__)
api = Api(app)

#coordDest=setDest('Valence')
#coordSrc=setSource('Paris')
@app.route("/")
def index():
    return render_template("index.html");


@app.route('/test',methods=['GET', 'POST'])
def resultat():
    duree=''
    if request.method == 'GET':
        return render_template("index.html")
    else:             
        depart = request.form.get("depart")
        arrivee = request.form.get("arrivee")
        #sautonmy = request.form.get("autonomy")
        margin = int(request.form.get("margin"))
        autonmy = int(request.form.get("autonomy")) 
        coordDestA=getCoord(arrivee)
        coordSrcA=getCoord(depart)

        ## Control Prints :
        print(arrivee)
        print(depart)
        print(autonmy)
        print(margin)
        print('coordDest:',coordDestA)
        print('coordSrc :',coordSrcA)

        ## Duration of the trajectory
        duree=duration(coordDestA,coordSrcA)

        ## Coord for bornes && distance
        coordDest=requestCoordonates(arrivee)
        coordSrc=requestCoordonates(depart)
        coord2 = str(coordSrc.latitude)+","+str(coordSrc.longitude)+";"+str(coordDest.latitude)+","+str(coordDest.longitude)
        distance=distanceCalculator(coord2)
        bornes=trajectory(coordSrc.latitude, coordSrc.longitude, coordDest.latitude, coordDest.longitude, autonmy, margin, distance)
        #steps=trajectory(depart,arrivee,autonmy,margin)
        ## the results on the site
        
        return render_template("index.html",depart=depart,arrivee=arrivee, duree=duree, coord2=coord2, distance=distance, bornes=bornes)

if __name__ == "__main__":

    app.run(host='0.0.0.0', port=5000, debug=True)