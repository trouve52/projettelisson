# -*- coding: utf-8 -*-
"""
@author: ATR
"""

import requests
import json
from logging import info
import urllib.request, json
from geopy.geocoders import Nominatim
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)



def getCoord(city):
        # fonction 
        url = 'https://api-adresse.data.gouv.fr/search/?q='

        requete = url + city
        requete += "&postcode="
        rqt = requests.get(requete)
        
        infosAddress = rqt.json() 
        
        features = infosAddress["features"]
        features_list = features[0]
        geometry = features_list["geometry"]
        coordonees = geometry["coordinates"]
        
        latitude_dst = str(coordonees[1])
        longitude_dst = str(coordonees[0])
        coordDest=[longitude_dst,latitude_dst]
        return  coordDest


def  duration(latitudeA,longitudeA, latitudeB,longitudeB):
        r = requests.get(f"http://router.project-osrm.org/route/v1/car/{longitudeA},{latitudeA};{longitudeB},{latitudeB}?overview=false""")

        routes = json.loads(r.content)
        route_1 = routes.get("routes")[0]
        print('routes',routes)
        #print(route_1)
        time=(route_1["duration"])
        min=time/60
        heure=min/60
        print('min',min)
        print('heure',heure)
        return heure


def requestCoordonates(city):
    geolocator = Nominatim(user_agent="ATR")
    coord = geolocator.geocode(city)
    return coord


def distanceCalculator(coord2):
    Trajectorydistancereq = "http://router.project-osrm.org/route/v1/driving/"+coord2+"?overview=false"
    with urllib.request.urlopen(Trajectorydistancereq) as url:
        data = json.loads(url.read().decode())

    for items in data["routes"]:
        for elements in items["legs"]:
            distance = elements["distance"]
    distance = distance/1000
    return distance

def trajectory(latDep, longDep, latArr, longArr, carautonomy, marginseekingborne, distance):
    listStop = needBreak(latDep, longDep, latArr, longArr, distance, carautonomy, marginseekingborne)
    return listStop

def needBreak(latDep, longDep, latArr, longArr, dist, carautonomy, marginseekingborne ):

    distcar= carautonomy - marginseekingborne
    if dist < distcar:
        return 0
    else:
        nrbBreak = round(dist/distcar)
        latDist = latDep - latArr
        longDist = longDep - longArr

        latEtapedist = latDist / nrbBreak
        longEtapedist = longDist / nrbBreak

        nbr = 0
        listStop = ""
        while nrbBreak != 0:
            nbr += 1

            if latDep < latArr:
                latDep = latDep + latEtapedist
            else: 
                latDep = latDep - latEtapedist
            if longDep > longArr:
                longDep = longDep + longEtapedist
            else:
                longDep = longDep - longEtapedist
           
            listStop += "Arret n°"+str(nbr)+": \n\n"+str(nearBornes (latDep, longDep, 5000))+"\n\n"
            nrbBreak = nrbBreak - 1         
            return listStop       

def nearBornes(lat, long, peri):    
    # Récupération du json avec les params.
    with urllib.request.urlopen("https://opendata.reseaux-energies.fr/api/records/1.0/search/?dataset=bornes-irve&q=&facet=region&geofilter.distance="+str(lat)+"%2C"+str(long)+"%2C"+str(peri)+"") as url:
        data = json.loads(url.read().decode())
    
   # Vérification de la présence de bornes dans le périmetre.
    if data["nhits"] == 0:
        peri = peri + 5000
        ## au dessus , ça augmente la largeur de périmètre recherche de borne
        ## if peri > xx , mettre une limite au périmètre de recherche ==> 
        #infos = "Pas de borne à moins de "+peri+"km"
        #break 
        infos = nearBornes(lat, long, peri)
    else:
        infos = parsage(data)
        
    return infos ## return vers need break

def parsage(data):
    infos = ""
    for items in data["records"]:
        infos += str(items["fields"]["ad_station"])+" à "+str(round(float(items["fields"]["dist"])))+"m.\n"
        #print ("Distance vers borne  "+str(items["fields"]["dist"]))  si je veux distance vers borne, c ici (pour distance totale)
    return infos ## return to nearbornes